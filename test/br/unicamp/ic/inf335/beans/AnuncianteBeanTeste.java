package br.unicamp.ic.inf335.beans;


import static org.junit.jupiter.api.Assertions.assertEquals;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;

import org.junit.Test;

public class AnuncianteBeanTeste {

	@Test
	public void testCriacaoAnunciante() {
		ProdutoBean produto = new ProdutoBean("1", "Tênis Um", "Descrição do tênis um", 100.0, "Estado um");
		AnuncioBean anuncio = new AnuncioBean(produto, new ArrayList<URL>(), 0.1);

		ArrayList<AnuncioBean> anuncios = new ArrayList<>();
		anuncios.add(anuncio);

		AnuncianteBean anunciante = new AnuncianteBean("Anunciante Um", "123456789-09", anuncios);

		assertEquals(anunciante.getNome(), "Anunciante Um");
		assertEquals(anunciante.getCPF(), "123456789-09");
		assertEquals(anunciante.getAnuncios(), anuncios);
	}

	@Test
	public void testAddAnuncio() {
		AnuncianteBean anunciante = new AnuncianteBean("Anunciante Um", "123456789-09", new ArrayList<AnuncioBean>());

		ProdutoBean produto = new ProdutoBean("1", "Tênis Um", "Descrição do tênis um", 100.0, "Estado um");
		AnuncioBean anuncio = new AnuncioBean(produto, new ArrayList<URL>(), 0.1);

		anunciante.addAnuncio(anuncio);

		assertEquals(1,anunciante.getAnuncios().size());
		assertEquals("1", anunciante.getAnuncios().get(0).getProduto().getCodigo());
	}

	@Test
	public void testRemoveAnuncio() {
		AnuncianteBean anunciante = new AnuncianteBean("Anunciante Um", "123456789-09", new ArrayList<AnuncioBean>());

		ProdutoBean produto = new ProdutoBean("1", "Tênis Um", "Descrição do tênis um", 100.0, "Estado um");
		AnuncioBean anuncio = new AnuncioBean(produto, new ArrayList<URL>(), 0.1);

		anunciante.addAnuncio(anuncio);
		anunciante.removeAnuncio(0);

		assertEquals(anunciante.getAnuncios().size(), 0);
		assertEquals(Collections.EMPTY_LIST, anunciante.getAnuncios());
	}

	@Test
	public void testValorMedioAnuncios() {
		AnuncianteBean anunciante = new AnuncianteBean("Anunciante Um", "123456789-09", new ArrayList<AnuncioBean>());

		ProdutoBean produto1 = new ProdutoBean("1", "Tênis Um", "Descrição do tênis um", 100.0, "Estado um");
		AnuncioBean anuncio1 = new AnuncioBean(produto1, new ArrayList<URL>(), 0.0);
		anunciante.addAnuncio(anuncio1);

		ProdutoBean produto2 = new ProdutoBean("2", "Tênis Dois", "Descrição do tênis dois", 120.0, "Estado dois");
		AnuncioBean anuncio2 = new AnuncioBean(produto2, new ArrayList<URL>(), 0.0);
		anunciante.addAnuncio(anuncio2);

		ProdutoBean produto3 = new ProdutoBean("3", "Tênis Três", "Descrição do tênis três", 113.0, "Estado três");
		AnuncioBean anuncio3 = new AnuncioBean(produto3, new ArrayList<URL>(), 0.0);
		anunciante.addAnuncio(anuncio3);

		assertEquals(anunciante.getAnuncios().size(), 3);
		assertEquals((100.0 + 120.0 + 113.0) / 3, anunciante.valorMedioAnuncios());
	}

	@Test
	public void testValorMedioAnunciosComArredondamento() {
		AnuncianteBean anunciante = new AnuncianteBean("Anunciante Um", "123456789-09", new ArrayList<AnuncioBean>());

		ProdutoBean produto1 = new ProdutoBean("1", "Tênis Um", "Descrição do tênis um", 100.0, "Estado um");
		AnuncioBean anuncio1 = new AnuncioBean(produto1, new ArrayList<URL>(), 0.1);
		anunciante.addAnuncio(anuncio1);

		ProdutoBean produto2 = new ProdutoBean("2", "Tênis Dois", "Descrição do tênis dois", 120.0, "Estado dois");
		AnuncioBean anuncio2 = new AnuncioBean(produto2, new ArrayList<URL>(), 0.1);
		anunciante.addAnuncio(anuncio2);

		ProdutoBean produto3 = new ProdutoBean("3", "Tênis Três", "Descrição do tênis três", 113.83, "Estado três");
		AnuncioBean anuncio3 = new AnuncioBean(produto3, new ArrayList<URL>(), 0.1);
		anunciante.addAnuncio(anuncio3);

		assertEquals(anunciante.getAnuncios().size(), 3);
		assertEquals( 100.15, anunciante.valorMedioAnuncios());
	}
}
